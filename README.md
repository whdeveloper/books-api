# Book API
### TOP Challenge
Gebruik je favoriete framework en libraries om een service te maken met een endpoint om boeken te zoeken. Het moet mogelijk zijn te **zoeken op tekst** en te kunnen **filteren op taal**. Het aantal resultaten dient **maximaal 10** te zijn, en **gesorteerd op publicatiedatum**.
De response bevat;
- De titel
- Auteur(s)
- ISBN
- Publicatiedatum
  - Gebruik volledige maandnamen in het Nederlands, zodanig dat een voorbeeld van een publicatiedatum is:  "12 maart 2022".

**Maak gebruik van de Google Books API.**

Neem in beschouwing:
- API documentatie
- Foutafhandeling / resilience

Beschrijf hoe je service gestart en gebruikt kan worden. En beschrijf kort de reden voor bepaalde keuzes die je maakt van framework tot oplossing.

Bonuspunten:
- Dockerfile

### Running
When running this project locally, maven is required. The command to start the application is `mvn spring-boot:run`. It is also possible to build and run this program with docker, an image can be build with `docker build .` or a container can be started with the docker compose file with the command `docker compose up`.

### Dependencies
- spring-boot-starter-web

Enables web applications through annotations with convention over configuration.

- google-api-services-books

Supplies an api-client & api helpers for the Google Books API.

### Documentation
To document this project, [Springfox](https://springfox.github.io/springfox/docs/current/) is used. The web interface for Springfox can be reached locally on [http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/) or when running this with docker [http://books-api:8080/swagger-ui/](http://books-api:8080/swagger-ui/)