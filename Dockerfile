# Build
FROM maven:3.6.3 AS maven
LABEL MAINTAINER="whdeveloper@gmail.com"
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN mvn package

# Run
FROM adoptopenjdk/openjdk11:alpine-jre
ARG JAR_FILE=books-api.jar
WORKDIR /opt/app
COPY --from=maven /usr/src/app/target/${JAR_FILE} /opt/app/
ENTRYPOINT ["java","-jar","books-api.jar"]