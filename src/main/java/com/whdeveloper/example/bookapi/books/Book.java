package com.whdeveloper.example.bookapi.books;

import java.util.List;

public class Book {
    private String title;
    private List<String> authors;
    private List<Isbn> isbn;
    private String publicationDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public List<Isbn> getIsbn() {
        return isbn;
    }

    public void setIsbn(List<Isbn> isbn) {
        this.isbn = isbn;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }
}
