package com.whdeveloper.example.bookapi.books;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.books.v1.Books;
import com.google.api.services.books.v1.BooksRequestInitializer;
import com.google.api.services.books.v1.model.Volumes;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.stream.Collectors;

public class BooksHelper {

    private static final String PROP_APPLICATION_NAME = "application_name";
    private static final String PROP_API_KEY = "api_key";

    private static final DateTimeFormatter DATE_FORMATTER_PUBLISHEDDATE = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
            .withLocale(Locale.forLanguageTag("NL-nl"));

    /**
     * Accesses the google books API.
     *
     * @param query        Search for volumes that contain this text string. There are special keywords you can specify in the search terms to search in particular fields, such as:</li>
     *                     <ul>
     *                          <li>intitle: Returns results where the text following this keyword is found in the title.</li>
     *                          <li>inauthor: Returns results where the text following this keyword is found in the author.</li>
     *                          <li>inpublisher: Returns results where the text following this keyword is found in the publisher.</li>
     *                          <li>subject: Returns results where the text following this keyword is listed in the category list of the volume.</li>
     *                          <li>isbn: Returns results where the text following this keyword is the ISBN number.</li>
     *                          <li>lccn: Returns results where the text following this keyword is the Library of Congress Control Number.</li>
     *                          <li>oclc: Returns results where the text following this keyword is the Online Computer Library Center number.</li>
     *                     </ul>
     * @param langRestrict Restrict results to books with this language.
     * @param maxResults   Maximum number of results to return. Acceptable values are 0 to 40, inclusive.
     * @param orderBy      Sort search results.
     * @return List of books.
     * @throws GeneralSecurityException Thrown when GoogleNetHttpTransport.newTrustedTransport() fails.
     * @throws IOException              When an IOException is thrown by the api client.
     */
    public static List<Book> queryGoogleBooks(String query, String langRestrict, Long maxResults, OrderBy orderBy) throws GeneralSecurityException, IOException {
        if (query == null || query.isEmpty()) {
            throw new IllegalArgumentException();
        }

        Properties properties = new Properties();
        properties.load(BooksHelper.class.getResourceAsStream("/secrets/api_key.properties"));
        final Books books = new Books.Builder(GoogleNetHttpTransport.newTrustedTransport(), GsonFactory.getDefaultInstance(), null)
                .setApplicationName(properties.getProperty(PROP_APPLICATION_NAME))
                .setGoogleClientRequestInitializer(new BooksRequestInitializer(properties.getProperty(PROP_API_KEY)))
                .build();

        Books.Volumes.List volumesList = books.volumes().list(query);
        if (langRestrict != null) {
            volumesList.setLangRestrict(langRestrict);
        }
        if (maxResults != null) {
            volumesList.setMaxResults(maxResults);
        }
        if (orderBy != null) {
            volumesList.setOrderBy(orderBy.getOrderBy());
        }

        Volumes volumes = volumesList.execute();
        if (volumes.getTotalItems() == 0 || volumes.getItems() == null) {
            return Collections.emptyList();
        }

        return volumes.getItems().stream().map(volume -> {
            Book book = new Book();
            book.setTitle(volume.getVolumeInfo().getTitle());
            book.setIsbn(volume.getVolumeInfo().getIndustryIdentifiers().stream()
                    .map(industryIdentifier -> {
                        Isbn isbn = new Isbn();
                        isbn.setType(industryIdentifier.getType());
                        isbn.setIdentifier(industryIdentifier.getIdentifier());
                        return isbn;
                    }).collect(Collectors.toList()));
            book.setAuthors(volume.getVolumeInfo().getAuthors());
            String date = volume.getVolumeInfo().getPublishedDate();
            try {
                LocalDate parsedDate = LocalDate.parse(date);
                date = DATE_FORMATTER_PUBLISHEDDATE.format(parsedDate);
            } catch (DateTimeParseException ignored) {
                // Not the standard dd-MM-yyyy format, probably a short version. Do not change the original
            }
            book.setPublicationDate(date);
            return book;
        }).collect(Collectors.toList());
    }

    public enum OrderBy {
        /**
         * <li>Most recently published.</li>
         */
        NEWEST("newest"),
        /**
         * <li>Relevance to search terms.</li>
         */
        RELEVANCE("relevance");

        private final String orderBy;

        OrderBy(String orderBy) {
            this.orderBy = orderBy;
        }

        public String getOrderBy() {
            return orderBy;
        }
    }
}