package com.whdeveloper.example.bookapi.controller;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.whdeveloper.example.bookapi.books.Book;
import com.whdeveloper.example.bookapi.books.BooksHelper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BooksController {

    @GetMapping
    @ApiOperation("Retrieve a list of books from the Google Books API")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved books"),
            @ApiResponse(code = 500, message = "Failed to retrieve books")
    })
    public List<Book> getBooks(
            @ApiParam("Query to search books with.") @RequestParam String query,
            @ApiParam("Language code to filter the books on.") @RequestParam(required = false) String langRestrict
    ) throws GeneralSecurityException, IOException {
        return BooksHelper.queryGoogleBooks(query, langRestrict, 10L, BooksHelper.OrderBy.NEWEST);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({GeneralSecurityException.class, IOException.class, GoogleJsonResponseException.class})
    public String handleGeneralSecurityException() {
        return "Failed to query the google books api";
    }
}
